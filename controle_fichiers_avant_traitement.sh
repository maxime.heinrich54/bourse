#!/bin/bash
#contrôle des fichiers avant traitement
#les fichiers portefeuille sont à contrôler (doublons et champs vides)
#le fichier des ventes est à contrôler (doublons et champs vides)


chemin_projet=""
fichier_vente="$chemin_projet/fichier_ventes.csv"


#fonction de contrôle des fichiers (en parametre le fichier à contrôler)
controle_fichier(){
	if [[ $# -lt 1 ]]
	then
		echo "il manque le paramètre \"nom du fichier\""
		exit 1
	fi
	fichier="$1"
	
	#controle doublons
	doublons="$(cat $fichier |cut -d';' -f1 |sort |uniq -d)"
	if [[ "$doublons" == "" ]]
	then
		echo "pas de doublons dans le fichier $fichier"
	else
		echo "doublons dans le fichier $fichier : $doublons"
		CR=1
	fi
	
	#controle champs vides
	champs_vides="$(cat $fichier |grep -e ";;" -e "^;" -e ";$" )"
	if [[ "$champs_vides" == "" ]]
	then
		echo "pas de champs vides dans le fichier $fichier"
	else
		echo "champs vides dans le fichier $fichier : $champs_vides"
		CR=1
	fi


}

CR=0

#contrôle des doublons et des champs vides dans les fichiers portefeuille
for fichier_portefeuille in $(ls -d $chemin_projet/*/fichier_portefeuille.csv)
do
	controle_fichier $fichier_portefeuille
done

#contrôle des doublons et des champs vides dans le fichier des ventes
controle_fichier $fichier_vente

#sortie en erreur si un des controle a échoué
if [[ $CR == 1 ]]
then
	echo "*** ERREUR *** : Erreur lors d'au moins un contrôle"
else
	exit 0
fi
