#!/bin/bash

if [[ $# -lt 2 ]]
then
	echo "mauvais paramètres, syntaxe : reinit_bots bot solde"
	exit 1
fi

> /home/maxime/bourse/$1/fichier_portefeuille.csv
> /home/maxime/bourse/$1/historique_transactions.csv
echo $2 > /home/maxime/bourse/$1/fichier_solde.csv

