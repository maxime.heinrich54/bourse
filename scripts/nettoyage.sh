#!/bin/bash
#contrôle d'intégrité des fichiers et archivage

chemin_projet=""
liste_bots=""

#controle des fichiers_portefeuille
for bot in $liste_bots
do
	fichier_portefeuille="$(ls $chemin_projet/$bot/fichier_portefeuille.csv)"
	for action in $fichier_portefeuille
	do
		if [[ "$(grep "$action" $fichier_portefeuille)" -ne 1 ]]
		then
			echo "*** ERREUR ***: action $action apparait en plusieurs exemplaires"
		fi	
	done
done

#zippage des fichiers, une archive par jour
#le zippage se fait à condition que 2 fichiers restent
liste_fichiers="$(ls $chemin_projet/historique/202*.csv)"
liste_dates="$(echo $liste_fichiers |cut -c1-8)"
date_avant_dernier_fichier="$(ls $chemin_projet/historique/202*.csv |tail -2 |head -1 |cut -c1-8)"
date_dernier_fichier="$(ls $chemin_projet/historique/202*.csv |tail -1 |cut -c1-8)"

if [[ "$liste_fichiers" == "" ]]
then
	echo "pas de fichiers à zipper"
else
	#le zippage ne peut se faire que s'il y a au moins 2 date
	for date in $liste_dates
	do
		if [[ "$date" != "$date_dernier_fichier" ]]&&[[ "$date" != "$date_avant_dernier_fichier" ]]
		then
			echo "zippage des fichiers avec la date $date"
			zip $chemin_projet/historique/$date*.csv
			
		else
			echo "la date $date est celle d'un des 2 derniers fichiers"
		fi
	done
fi
