#!/bin/bash
#classe principale, organise les lancements de tous les autres shells
#par défaut pas de paramètres, "refreshonly" pour activer le monde refreshonly (actualisation des cours sans achat/vente)

liste_bots="bot_classique bot_seuils_reduits bot_solde_10000"
chemin_projet="/home/maxime/bourse/"
refreshonly="false"
fichier_logs="$chemin_projet/logs/commun.log"

#import des fonctions mathematiques
. $chemin_projet/scripts/mathematiques.sh

echoLOG(){
        if [[ "$fichier_logs" == "" ]]
        then
                echo "nom du fichier de log non défini, sortie en erreur"
                exit 1
        fi
        texte="$*"
        echo "$(date +%Y-%m-%d'||'%H:%M:%S)||$texte" >> $fichier_logs
}

#actualisation des cours uniquement
if [[ "$1" == "refreshonly" ]]
then
	refreshonly="true"
fi

#1- Recuperation des cours et de la MM100
echo "*** ETAPE 1 *** : lancement de la recuperation des cours"
bash $chemin_projet/scripts/recuperation_cours.sh

#2- Récupération des MM100
echo "*** ETAPE 2 *** : lancement de la récupération des MM100"
bash $chemin_projet/scripts/recuperation_mm100.sh

#3- Recensement des cours venant de franchir la MM100 vers le haut
echo "*** ETAPE 3 *** : lancement du script de recensement des actions à acheter"
bash $chemin_projet/scripts/franchissement_MM100.sh

#si le mode refreshonly est actiivé, on s'arrête là
if [[ "$refreshonly" == "true" ]]
then
	echo "mode refreshonly activé, exécution terminée"
	echoLOG "Exécution terminée : mode refreshonly"
	exit 0
fi

#les traitements suivants sont à effectuer pour chaque bot
for bot in $liste_bots
do

	#4- Verification des seuils pour chaque portefeuille
	echo "*** ETAPE 4-$bot *** : lancement du script de verification des seuils pour $bot"
	bash $chemin_projet/scripts/verification_seuils.sh ${bot}

	#5- Achat/vente avec actualisation des soldes, des portefeuilles et de l'historique
	echo "*** ETAPE 5-$bot *** : lancement du script d'achat/vente pour $bot"
	bash $chemin_projet/scripts/achat_vente.sh ${bot}

done

#6- Génération du bilan
echo "*** ETAPE 6 *** : génération du bilan de l'exécution"
bash $chemin_projet/scripts/bilan.sh $liste_bots

#7- Commit des fichiers de logs
echo "*** ETAPE 7 ***: commit des fichiers de logs"
bash $chemin_projet/scripts/git_commit_fichiers_courants.sh

echoLOG "Exécution terminée : mode normal"
echo "Fin du script"

