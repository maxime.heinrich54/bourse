#!/bin/bash
#script pour faciliter l'utilisation des opérations mathématiques

#addition
function addition () {
	if [[ $# -ne 2 ]]
	then
		echo "mauvaise utilisation de l'addition : 2 paramètres à renseigner, paramètres actuellement renseignés : $0"
		exit 1
	fi
	premier_nombre=$1
	deuxieme_nombre=$2

	echo "${premier_nombre}+${deuxieme_nombre}" |bc -l
	exit 0
}

#soustraction
function soustraction () {
	if [[ $# -ne 2 ]]
	then
		echo "mauvaise utilisation de la soustraction : 2 paramètres à renseigner, paramètres actuellement renseignés : $0"
		exit 1
	fi
	premier_nombre=$1
	deuxieme_nombre=$2

	echo "${premier_nombre}-${deuxieme_nombre}" |bc -l
	exit 0
}

#multiplication
function multiplication () {
	if [[ $# -ne 2 ]]
	then
		echo "mauvaise utilisation de la multiplication : 2 paramètres à renseigner, paramètres actuellement renseignés : $0"
		exit 1
	fi
	premier_nombre=$1
	deuxieme_nombre=$2

	echo "${premier_nombre}*${deuxieme_nombre}" |bc -l
	exit 0
}

#division
function division (){
	if [[ $# -ne 2 ]]
	then
		echo "mauvaise utilisation de la division : 2 paramètres à renseigner, paramètres actuellement renseignés : $0"
		exit 1
	fi
	premier_nombre=$1
	deuxieme_nombre=$2

	echo "${premier_nombre}/${deuxieme_nombre}" |bc -l
	exit 0
}
