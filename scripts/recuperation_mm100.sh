#!/bin/bash
#recuperation de la MM100 sur la page de detail de boursorama
exit 0
chemin_fichier="/home/maxime/bourse/"
liste_actions="$chemin_fichier/liste_actions.txt"
fichier_mm100="$chemin_fichier/fichier_mm100.csv"
> "$fichier_mm100"
OLDIFS=$IFS
IFS=$'\n'
for action in $(cat $liste_actions |cut -d';' -f1)
do
	code_action="$(grep "$action" $chemin_fichier/liste_actions.txt |cut -d';' -f2)"
	if [[ "$code_action" == "" ]]
	then
        	echo "*** ERREUR *** : Erreur lors de la récupération du code de l'action"
        	exit 1
	fi
	data_action="$(curl -s "https://www.boursorama.com/cours/$code_action/")"
	mm100="$(echo "$data_action" |grep -A6 MM100 |grep "[0-9]\.[0-9]" |awk -F' ' '{print $1$2}' |cut -d'<' -f1)"
	if [[ "$( echo "$mm100" |grep -v [a-z] |grep -v [A-Z] )" != "" ]]
	then
	        echo "INFO : action : $action, MM100 : $mm100"
		echo "$action;$mm100" >> $fichier_mm100
	else
	        echo "*** ERREUR *** : Erreur lors de la récupération de la MM100 pour l'action $1 (code $code_action)"
	        exit 1
	fi
done

IFS=$OLDIFS
