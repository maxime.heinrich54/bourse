#!/bin/bash
#1- Recuperation des cours et de la MM100
#les cours sont stockés dans un fichier dernier_cours_connus.csv commun à tous les bots et dans un fichier horodaté
#format du fichier dernier_cours_connu.csv:"nom de l'action;cours actuel"
#format du fichier liste_actions.txt:"nom de l'action;code de l'action"
#format des fichiers horodates:"nom de l'action;cours de l'action"

chemin_projet="/home/maxime/bourse/"
liste_actions="$chemin_projet/liste_actions.txt"
fichier_dernier_cours="$chemin_projet/historique/dernier_cours_connu.csv"
fichier_horodate="$chemin_projet/historique/$(date +%Y%m%d%H).csv"
page1="$(curl -s https://www.boursorama.com/bourse/actions/cotations/?quotation_az_filter%5Bmarket%5D=1rPCAC)"
page2="$(curl -s https://www.boursorama.com/bourse/actions/cotations/page-2?quotation_az_filter%5Bmarket%5D=1rPCAC)"

if [[ -e "$fichier_horodate" ]]
then
        echo "le fichier existe déjà, pas d'ecriture"
        ecriture="non"
	exit 0
else
	ecriture="oui"
fi

OLDIFS=$IFS
IFS=$'\n'
for nom_action in $(cat $liste_actions |cut -d';' -f1)
do
	cours_action="$(echo "$page1" |grep -i -A1 "$nom_action" |awk -F'data-ist-last' '{print $2}' |cut -d'>' -f2  |cut -d'<' -f1 |grep -v ^$ |sed 's/\./,/g' |sed 's/\ //g')"
        if [[ "$cours_action" == "" ]]
        then
                cours_action="$(echo "$page2" |grep -i -A1 "$nom_action" |awk -F'data-ist-last' '{print $2}' |cut -d'>' -f2  |cut -d'<' -f1 |grep -v ^$ |sed 's/\./,/g' |sed 's/\ //g')"
        fi
	echo "$nom_action;$cours_action"
	echo "$nom_action;$cours_action" >> "$fichier_dernier_cours"
	echo "$nom_action;$cours_action" >> "$fichier_horodate"

done
IFS=$OLDIFS
