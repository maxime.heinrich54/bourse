#!/bin/bash
#5- Génération du bilan de l'exécution
#PARAMETRE : liste des bots
chemin_projet="/home/maxime/bourse/"

liste_bots="$1 $2 $3"
for bot in $liste_bots
do
	fichier_solde="$chemin_projet/$bot/fichier_solde.csv"
	solde="$(cat $fichier_solde)"
	fichier_portefeuille="$chemin_projet/$bot/fichier_portefeuille.csv"
	portefeuille="$(cat $fichier_portefeuille)"
	echo "le bot $bot a un solde de $solde et son portefeuille est :"
	echo "${portefeuille}"
done
