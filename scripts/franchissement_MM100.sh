#!/bin/bash
#2- Recensement des cours venant de franchir la MM100 vers le haut
#les actions ayant franchis la MM100 vers le haut sont considérés comme "à acheter" et sont stockées dans le fichier fichier_achat.csv commun à tous les bots
#la comparaison se fait entre les 2 derniers fichiers horodatés ($chemin/projet/historique)
#sécurité : l'écart entre la MM100 et le cours de l'action doit être faible ("ecart.maximum.mm100" à ajouter dans les properties)
#format du fichier fichier_achat.csv:"nom de l'action;dernier cours connu"
#format du fichier dernier_cours_connu.csv:"nom de l'action;cours actuel;mm100"
#format du fichier liste_actions.txt:"nom de l'action;code de l'action"
#format des fichiers horodates:"nom de l'action;cours de l'action"

chemin_projet="/home/maxime/bourse/"
liste_actions="$chemin_projet/liste_actions.txt"
dernier_cours_connu="$chemin_projet/historique/dernier_cours_connu.csv"
dernier_fichier_horodate="$chemin_projet/historique/$( ls -lrt "$chemin_projet/historique/" |grep -v dernier |tail -1 |awk -F' ' '{print $NF}')"
avant_dernier_fichier_horodate="$chemin_projet/historique/$(ls -lrt "$chemin_projet/historique/" |grep -v dernier |tail -2 |head -1 |awk -F' ' '{print $NF}')"
fichier_dernier_cours="$chemin_projet/historique/dernier_cours_connu.csv"
fichier_achat="$chemin_projet/fichier_achat.csv"
> "$fichier_achat"
fichier_mm100="$chemin_projet/fichier_mm100.csv"

OLDIFS=$IFS
IFS=$'\n'
for action in $(cat $liste_actions |cut -d';' -f1)
do
	mm100="$(grep "$action" $fichier_mm100 |cut -d';' -f2 )"
	echo "action $action, mm100 $mm100"
	#recuperation des 2 derniers cours
	dernier_cours="$(grep "$action" $dernier_fichier_horodate |cut -d';' -f2)"
	avant_dernier_cours="$(grep "$action" $avant_dernier_fichier_horodate |cut -d';' -f2)"

	if [[ $(echo "$dernier_cours" "$mm100"  | awk '{print ($1 > $2)}' ) == 1 ]]&&[[ $(echo "$mm100" "$avant_dernier_cours"  | awk '{print ($1 > $2)}') == 1 ]]
	then
		echo "l'action $action vient de franchir la MM100 (cours actuel : $dernier_cours | cours précédent : $avant_dernier_cours)"
		echo "$action;$dernier_cours" >> "$fichier_achat"
	fi

done
IFS=$OLDIFS
