#!/bin/bash
#4- Achat/vente avec actualisation des soldes, des portefeuilles et de l'historique
#PARAMETRE : $bot (nom du bot)
#format du fichier fichier_portefeuille.csv:"nom de l'action;nombre d'actions;prix d'achat de l'action"
#format du fichier fichier_achat.csv:"nom de l'action;prix d'achat de l'action"
#format du fichier fichier_vente.csv:"nom de l'action;dernier cours connu de l'action"
#format du fichier fichier_historique.csv:"nom de l'action;ACHAT ou VENTE;nombre d'actions concernées;montant de la transaction"
#format du fichier fichier_solde.csv:"solde"

chemin_projet="/home/maxime/bourse/"
bot="$1"
fichier_historique="$chemin_projet/$bot/historique_transactions.csv"
fichier_solde="$chemin_projet/$bot/fichier_solde.csv"
fichier_achat="$chemin_projet/fichier_achat.csv"
fichier_vente="$chemin_projet/$bot/fichier_vente.csv"
fichier_portefeuille="$chemin_projet/$bot/fichier_portefeuille.csv"
fichier_logs="$chemin_projet/logs/$bot.log"

echoLOG(){
        if [[ "$fichier_logs" == "" ]]
        then
                echo "nom du fichier de log non défini, sortie en erreur"
                exit 1
        fi
        texte="$*"
        echo "$(date +%Y-%m-%d'||'%H:%M:%S)||$texte" >> $fichier_logs
}

#vente
if [[ "$(cat $fichier_vente)" == "" ]]
then
	echo "pas d'action à vendre"
	echoLOG "pas d'action à vendre pour $bot"
else
	for action in $(cat $fichier_vente |cut -d';' -f1)
	do
		nom_action="$(cat $fichier_vente |grep "$action"|cut -d';' -f1 |head -1)"
		cours_action="$(cat $fichier_vente |grep "$nom_action" |cut -d';' -f2 |head -1)"


		nombre_action="$(cat $fichier_portefeuille |grep "$nom_action" |cut -d';' -f2 |head -1)"
		montant_transaction="$(echo "$cours_action*$nombre_action" |bc)"
		echo "vente de $nom_action pour $cours_action"
		sed "/$nom_action/d" $fichier_portefeuille

		#historisation
		echo "$nom_action;VENTE;$nombre_action;$montant_transaction" >> "$fichier_historique"
		#mise a jour du solde
		solde="$(cat $fichier_solde)"
		nouveau_solde="$(echo "$solde+$montant_transaction" |bc -l)" 
		echo "le nouveau solde est $nouveau_solde"
		echo "$nouveau_solde" > "$fichier_solde"
	done
fi


#achat
if [[ "$(cat $fichier_achat)" == "" ]]
then
	echo "pas d'action à acheter"
	echoLOG "pas d'action à acheter pour $bot"
else
        for action in $(cat $fichier_achat |cut -d';' -f1)
        do
			solde="$(cat $fichier_solde)"
			nom_action="$(cat $fichier_achat |grep "$action" |cut -d';' -f1)"
			cours_action="$(cat $fichier_achat |grep "$action" |cut -d';' -f2)"
			nombre_action=0
			if [[ "$(echo "$solde" "$cours_action" | awk '{print ($1 > $2)}' )" == 1 ]]
			then
				nombre_action="$(echo $(echo "$solde/4" |bc) |cut -d'.' -f1)" 
				if [[ $nombre_action -eq 0 ]]
				then
					nombre_action="1"
				fi
				echo "achat de $nombre_action action(s) de $nom_action par $bot"

				montant_transaction="$(echo "$cours_action*$nombre_action"|bc -l)"
				echo "achat de $nom_action pour $cours_action"
				echo "$nom_action;$nombre_action;$cours_action" >> $fichier_portefeuille
				#historisation
				echo "$nom_action;ACHAT;$nombre_action;$montant_transaction" >> "$fichier_historique"
				#mise a jour du solde
				nouveau_solde="$(echo "$solde-$montant_transaction" |bc -l)"
				echo "le nouveau solde est $nouveau_solde"
				echo "$nouveau_solde" > $fichier_solde

			else
				echo "l action $nom_action est trop chere pour $bot (solde : $solde)"
			fi

		done
fi
