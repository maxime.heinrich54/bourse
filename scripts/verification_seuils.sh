#!/bin/bash
#3- Verification des seuils pour chaque portefeuille
#PARAMETRE : $bot (le nom du bot)
#si une action passe un seuil haut ou bas, elle est considérée comme "à vendre" et est stockée dans un fichier fichier_vente.csv pour chaque bot
#format du fichier fichier_portefeuille.csv:"nom de l'action;nombre d'actions;prix d'achat de l'action"
#format du fichier_vente.csv:"nom de l'action;dernier cours connu de l'action"

chemin_projet="/home/maxime/bourse/"
bot="$1"
fichier_portefeuille="$chemin_projet/$bot/fichier_portefeuille.csv"
fichier_vente="$chemin_projet/$bot/fichier_vente.csv"
fichier_logs="$chemin_projet/logs/$bot.log"

echoLOG(){
	if [[ "$fichier_logs" == "" ]]
	then
		echo "nom du fichier de log non défini, sortie en erreur"
		exit 1
	fi
	texte="$*"
	echo "$(date +%Y-%m-%d'||'%H:%M:%S)||$texte" >> $fichier_logs
}

OLDIFS=$IFS
IFS=$'\n'
for action_portefeuille in $(cat $fichier_portefeuille |cut -d';' -f1)
do

	pourcentage_seuil_haut="$(grep $bot.pourcentage.seuil.haut $chemin_projet/properties/commun.properties |cut -d'=' -f2)"
	pourcentage_seuil_bas="$(grep $bot.pourcentage.seuil.bas $chemin_projet/properties/commun.properties |cut -d'=' -f2)"

	#recuperation du cours de l'action
	cours_action="$(cat $chemin_projet/historique/dernier_cours_connu.csv |grep "$action_portefeuille" |cut -d';' -f2)"

	#recuperation du nombre d'actions $action_portefeuille dans le portefeuille
	nombre_action="$(cat $fichier_portefeuille |cut -d';' -f2)"

	echo "surveillance de l'action : $action_portefeuille (actuellement $nombre dans le portefeuille du bot $bot)"
	prix_achat_action="$(cat $fichier_portefeuille |grep $action_portefeuille |cut -d';' -f3 |sed 's/,/\./g' |head -1)"

	echo "prix_achat_action : $prix_achat_action | pourcentage_seuil_haut : $pourcentage_seuil_haut | pourcentage_seuil_bas : $pourcentage_seuil_bas"
	#calcul des seuils
	seuil_haut="$(echo "$prix_achat_action*$pourcentage_seuil_haut" |bc )"
	seuil_bas="$(echo "$prix_achat_action*$pourcentage_seuil_bas" |bc )"

	if [[ "$(echo "$cours_action" "$seuil_haut" |awk '{print ($1 > $2)}' )" == 1 ]]
	then
		echo "VENTE : Le seuil haut de $action_portefeuille a été dépassé (vente en profit)"
		echo "$action_portefeuille;$cours_action" >> "$fichier_vente"
		echoLOG "L'action $action_portefeuille a dépassé le seuil haut, inscription au fichier des ventes"
	fi
	if [[ "$(echo "$seuil_bas" "$cours_action" |awk '{print ($1 > $2)}' )" == 1 ]]
	then
		echo "VENTE : Le seuil bas de $action_portefeuille a été dépassé (vente à perte)"
		echo "$action_portefeuille;$cours_action" >> "$fichier_vente"
		echoLOG "L'action $action_portefeuille a dépassé le seuil bas, inscription au fichier des ventes"
	fi

done
IFS=$OLDIFS

